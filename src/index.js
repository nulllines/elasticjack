'use strict';

module.exports = {
    release: require('./eb-release'),
    ebupload: require('./eb-upload'),
    s3website: require('./s3-website'),
    version: require('./eb-version'),
    bundle: require('./eb-bundle'),
};
