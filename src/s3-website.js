'use strict';

var glob = require('glob');
var fs = require('fs');
var path = require('path');

var AWS = require('aws-sdk');
var s3 = new AWS.S3();

//var debug = require('debug')('elasticjack:s3upload');

module.exports = function (options) {

    return new Promise(function (resolve, reject) {

        if (options.prefix) {
            options.prefix = `${options.prefix}/`;
        }

        glob(options.glob, {cwd: options.path, nodir: true}, function (err, files) {
            if (err) {
                reject(err);
            } else {
                var upload = files.reduce(function (prev, file) {

                    var absolute = path.join(options.path, file);
                    var params = {
                        ContentType: 'text/html; charset=utf-8'
                    };

                    if (file.match(/.js$/)) {
                        params.ContentType = 'application/javascript; charset=utf-8';
                    }

                    if (file.match(/.css$/)) {
                        params.ContentType = 'text/css; charset=utf-8';
                    }

                    return prev.then(function () {
                        return s3
                            .putObject({
                                Bucket: options.bucket,
                                Body: fs.readFileSync(absolute),
                                Key: `${options.prefix}${file}`,
                                ServerSideEncryption: 'AES256',
                                ContentType: params.ContentType,
                                ACL: 'public-read'
                            })
                            .promise();
                    });
                }, Promise.resolve());

                resolve(upload);
            }
        });

    });
};

