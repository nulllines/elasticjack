'use strict';

var path = require('path');
var fs = require('fs');
var archiver = require('archiver');

module.exports = function (options) {

    var archive_path = path.join(require('os').tmpdir(), options.eb.version_key);

    var archive = archiver.create('zip', {});

    var output = fs.createWriteStream(archive_path);

    archive.pipe(output);
    archive.bulk(options.files);
    archive.finalize();

    return new Promise(function (resolve, reject) {

        output.on('close', function () {
            resolve({
                stream: archive,
                bytes: archive.pointer(),
                path: archive_path
            });
        });

        archive.on('error', function (err) {
            reject(err);
        });

    });
};
