'use strict';

var AWS = require('aws-sdk');
var debug = require('debug')('elasticjack:release');

module.exports = function (options) {

    var elasticbeanstalk = new AWS.ElasticBeanstalk({
        region: options.aws.region
    });

    var params = {
        ApplicationName: options.eb.application_name,
        EnvironmentName: `${options.eb.environment_name}`,
        VersionLabel: options.eb.version_label,
        OptionSettings: [
            {
                Namespace: 'aws:elasticbeanstalk:application:environment',
                OptionName: 'NODE_ENV',
                Value: options.eb.env
            },
            {
                Namespace: 'aws:autoscaling:launchconfiguration',
                OptionName: 'IamInstanceProfile',
                Value: `aws-elasticbeanstalk-ec2-role${options.aws.profile_suffix}`
            },
            /*{
                Namespace: 'aws:elasticbeanstalk:application:environment',
                OptionName: 'DEBUG',
                Value: `${options.app.namespace}:*`
            },*/
            {
                Namespace: 'aws:elasticbeanstalk:application:environment',
                OptionName: 'ROLE',
                Value: options.app.name
            },
            {
                Namespace: 'aws:cloudformation:template:resource:property',
                ResourceName: 'AWSEBAutoScalingLaunchConfiguration',
                OptionName: 'KeyName',
                Value: options.aws.keypair
            },
            {
                Namespace: 'aws:elasticbeanstalk:sns:topics',
                ResourceName: 'AWSEBAutoScalingLaunchConfiguration',
                OptionName: 'Notification Topic ARN',
                Value: `arn:aws:sns:${options.aws.region}:${options.aws.account_id}:elasticbeanstalk-${options.app.namespace}`
            }
        ]
    };

    return elasticbeanstalk
        .updateEnvironment(params)
        .promise();

};

