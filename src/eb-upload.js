'use strict';

var fs = require('fs');
var AWS = require('aws-sdk');
var s3 = new AWS.S3();

var debug = require('debug')('elasticjack:ebupload');

module.exports = function (options) {

    return new Promise(function (resolve, reject) {
        s3
            .upload({
                Bucket: options.eb.version_bucket,
                Body: fs.readFileSync(options.file),
                Key: options.eb.version_key,
                ServerSideEncryption: 'AES256'
            })
            .on('httpUploadProgress', function (evt) {
                debug('Bytes uploaded: %d', evt.loaded);
            })
            .on('error', function (err) {
                reject(err);
            })
            .send(function () {
                debug('Sended');
                resolve();
            });
    });
};

