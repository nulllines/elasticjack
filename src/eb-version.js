'use strict';

var AWS = require('aws-sdk');
var debug = require('debug')('elasticjack:version');

module.exports = function (options) {

    var elasticbeanstalk = new AWS.ElasticBeanstalk({
        region: options.aws.region
    });

    var params = {
        ApplicationName: options.eb.application_name,
        AutoCreateApplication: true,
        VersionLabel: options.eb.version_label,
        Description: '',
        SourceBundle: {
            S3Bucket: `elasticbeanstalk-${options.aws.region}-${options.aws.account_id}`,
            S3Key: options.eb.version_key
        }
    };

    return elasticbeanstalk
        .createApplicationVersion(params)
        .promise();

};
